from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

transactions = [
    {
        "id": 1,
        "type": "AIRTIME",
        "amount": 20,
        "payer": "0244242424",
        "payee": "0244343434",
        "network": "MTN",
        "recipient": "0244646464"
    }
]

accounts = [
    {
        "phone_number": "0244242424",
        "balance": 100.0
    },
    {
        "phone_number": "0244343434",
        "balance": 100.0
    },
    {
        "phone_number": "0244545454",
        "balance": 100.0
    },
]


class Airtime(Resource):
    def get(self):
        res = []
        for txn in transactions:
            if txn["type"] == "AIRTIME":
                res.append(txn)
        return res, 200

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("amount")
        parser.add_argument("buyer")
        parser.add_argument("network")
        parser.add_argument("recipient")
        args = parser.parse_args()

        payee = "0244343434"

        tid = len(transactions) + 1
        new_txn = {
            "id": tid,
            "type": "AIRTIME",
            "amount": args["amount"],
            "payer": args["buyer"],
            "payee": payee,
            "network": args["network"],
            "recipient": args["recipient"]
        }
        transactions.append(new_txn)

        for account in accounts:
            if account["phone_number"] == args["buyer"]:
                account["balance"] = account["balance"] - float(args["amount"])
            elif account["phone_number"] == payee:
                account["balance"] = account["balance"] + float(args["amount"])

        return new_txn, 201


class SendMoney(Resource):
    def get(self):
        res = []
        for txn in transactions:
            if txn["type"] == "SEND_MONEY":
                res.append(txn)
        return res, 200

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("amount")
        parser.add_argument("sender")
        parser.add_argument("receiver")
        args = parser.parse_args()

        tid = len(transactions) + 1
        new_txn = {
            "id": tid,
            "type": "SEND_MONEY",
            "amount": args["amount"],
            "payer": args["sender"],
            "payee": args["receiver"]
        }

        for account in accounts:
            if account["phone_number"] == args["sender"]:
                account["balance"] = account["balance"] - float(args["amount"])
            elif account["phone_number"] == args["receiver"]:
                account["balance"] = account["balance"] + float(args["amount"])

        transactions.append(new_txn)
        return new_txn, 201


class Account(Resource):
    def get(self):
        return accounts, 200


api.add_resource(Airtime, "/airtime")
api.add_resource(SendMoney, "/send_money")
api.add_resource(Account, "/account")

if __name__ == "__main__":
    app.run(port='5002', debug=True)
